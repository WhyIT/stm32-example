# STM32示例

#### 介绍
STM32示例

#### 软件架构
基于STM32CubeMX+Keil5+TOUCHGFX等软件平台


#### 安装教程

1.  安装keil5
2.  安装相应软件支持包
3.  安装stm32CubeMX

#### 使用说明

1.  可以看看某个代码模块的使用方法
2.  可以看看某个功能的实现算法
3.  可以关注CSDN：[(18条消息) 金_大虾的博客_CSDN博客-电源,单片机,Matlab领域博主](https://blog.csdn.net/qq_21835111?type=blog)


#### 注意

1. 每次更新小于100M

   
